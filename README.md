# Install

```shell
npm i expressjs-controller
```

# Quick start

```javascript
const express = require('express')
const boot = express()
const expressjsController = require("expressjs-controller");
const path = require("path");

const port = 3000;

boot.use(expressjsController.create(path.join(__dirname, "applications")));

boot.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
});
```

Place a file named index.js to `applications/index/index/` , the structure looks like this:
```text
boot.js
applications
--index
----index
------index.js
```

and the index.js content is:

```javascript
class Index {
    async get(req, res) {
        res.end("Hello World");
    }
}

exports.default = Index;
``` 

The controller action role is `/[application]/[controller]/[action]`,   
We can visit the page via `/` , `/index` , `/index/index` , `/index/index/index` 

We can also write an action with TypeScript, example here:  
```typescript
import IAction from "expressjs-controller/dist/interfaces/IAction";
import IRequest from "expressjs-controller/dist/interfaces/IRequest";
import IResponse from "expressjs-controller/dist/interfaces/IResponse";

export default class Index implements IAction {
    async get(req: IRequest, res: IResponse): Promise<any> {
        res.end("Hello TS");
    }
}
```
