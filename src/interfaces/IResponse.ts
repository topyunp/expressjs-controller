/*
Created on 2024/02/03
 */
import IRequest from "./IRequest";

export default interface IResponse {
    app: any;
    headersSent: boolean;
    locals: any;

    append(field: string, value?: string): any;

    attachment(filename?: string): any;

    cookie(name: string, value: string, options?: any): any;

    clearCookie(name: string, options?: any): any;

    download(path: string, filename?: string, options?: any, fn?: (err: any) => void): any;

    end(data?: any, encoding?: string): void;

    format(object: any): any;

    get(field: string): string;

    json(body: any): any;

    jsonp(body: any): any;

    links(links: any): any;

    location(path: string): any;

    req: IRequest;

    redirect(path: string): any;

    redirect(status: number, path: string): any;

    render(view: string, locals?: any, callback?: (err: any, html: any) => void): any;

    send(body: any): any;

    sendFile(path: string, options?: any, fn?: (err: any) => void): any;

    sendStatus(statusCode: number): any;

    status(code: number): IResponse;

    set(field: string, value?: string): any;

    type(type: string): any;

    vary(field: string): any;

}