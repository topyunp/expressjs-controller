/*
Created on 2024/02/03
 */

import IResponse from "./IResponse";

export default interface IRequest {
    app: any;
    baseUrl: string;
    body: any;
    cookies: any;
    fresh: boolean;
    hostname: string;
    ip: string;
    ips: Array<string>;
    method: string;
    originalUrl: string;
    params: any;
    path: string;
    protocol: string;
    query: any;
    res: IResponse;
    route: string;
    secure: boolean;
    signedCookies: any;
    stale: boolean;
    subdomains: Array<string>;
    xhr: boolean;
    args: Array<string>;

    accepts(types: string | Array<string>): any;

    acceptsCharsets(...charset: Array<string>): any;

    acceptsEncodings(...encoding: Array<string>): any;

    acceptsLanguages(...lang: Array<string>): any;

    get(field: string): string;

    is(type: string): string | boolean;

    range(size: number, options?: any): any;
}