import IRequest from "./IRequest";
import IResponse from "./IResponse";

export default interface IAction {
    get?: (req: IRequest, res: IResponse) => Promise<any>;
    post?: (req: IRequest, res: IResponse) => Promise<any>;
    head?: (req: IRequest, res: IResponse) => Promise<any>;
    options?: (req: IRequest, res: IResponse) => Promise<any>;
}