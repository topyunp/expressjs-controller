import IAction from "expressjs-controller/dist/interfaces/IAction";
import IRequest from "expressjs-controller/dist/interfaces/IRequest";
import IResponse from "expressjs-controller/dist/interfaces/IResponse";

export default class Index implements IAction {
    async get(req: IRequest, res: IResponse): Promise<any> {
        res.end("Hello TS");
    }
}