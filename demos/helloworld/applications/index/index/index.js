class Index {


    /**
     *
     * @param req {IRequest}
     * @param res {IResponse}
     * @returns {Promise<void>}
     */
    async get(req, res) {
        res.end("Hello World");
    }
}

exports.default = Index;