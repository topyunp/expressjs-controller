const express = require('express')
const boot = express()
const expressjsController = require("../../dist/index");
const path = require("path");

const port = 3000;

// respond with "hello world" when a GET request is made to the homepage
boot.use(expressjsController.create(path.join(__dirname, "apps")));

boot.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
});